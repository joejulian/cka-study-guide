# CKA Notes

## Getting Started

[Make life easier!](https://kubernetes.io/docs/user-guide/kubectl-cheatsheet/)

## Core Concepts (19%)

### Understand the Kubernetes API Primitives (~6.3%)

* Binding

  Binding, or Service Binding : a link between a Service Instance and an Application. It expresses the intent for an Application to reference and use a particular Service Instance.

  ``` yaml
  apiVersion: servicecatalog.k8s.io/v1alpha1
  kind: Binding
  metadata:
    name: johnsBinding
  spec:
    secretName: johnSecret
    ...Pod selector labels...
  ```
* [ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configmap/)
* [Endpoints](https://kubernetes.io/docs/concepts/services-networking/service/#services-without-selectors)
* [LimitRange](https://kubernetes.io/docs/tasks/administer-cluster/cpu-default-namespace)
  ``` yaml
  apiVersion: v1
  kind: LimitRange
  metadata:
    name: cpu-limit-range
  spec:
    limits:
    - default:
        cpu: 1
      defaultRequest:
        cpu: 0.5
      type: Container
  ```

  [Example use](https://github.com/kubernetes/kubernetes.github.io/blob/master/docs/admin/limitrange/limits.yaml)

* [Namespace](https://kubernetes.io/docs/tasks/administer-cluster/namespaces-walkthrough/)
* [Node](https://kubernetes.io/docs/concepts/architecture/nodes/)
* [PersistentVolume, PersistentVolumeClaim, StorageClass](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)
* [Pod](https://kubernetes.io/docs/user-guide/kubectl-cheatsheet/#creating-objects)
* [ReplicationController](https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/)
* [ResourceQuota](https://kubernetes.io/docs/concepts/policy/resource-quotas/#viewing-and-setting-quotas)
* [Secret](https://kubernetes.io/docs/concepts/configuration/secret/)
* [Service](https://kubernetes.io/docs/concepts/services-networking/service/)

### Understand the Kubernetes cluster Architecture (~6.3%)

![kubernetes architecture](https://storage.googleapis.com/static.ianlewis.org/prod/img/755/kubernetes-arch.png)

### Understand Service and other network primitives (~7.3%)

* [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)
* [NetworkPolicy](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
* [Service](https://kubernetes.io/docs/concepts/services-networking/service/)

## Cluster Maintenance (11%)

### Understand Kubernetes cluster upgrade process (~3.7)

* etcd2->etcd3

  stop control plane, upgrade etcd cluster, restart control plane using new apis

* control plane
  
  Upgrade api. Upgrade controller-manager and scheduler. Upgrade workers (kubelet and kube-proxy).

### Facilitate operating system upgrades (~3.7)

[Node mainentance](https://kubernetes.io/docs/tasks/administer-cluster/cluster-management/#maintenance-on-a-node)

### Implement backup and restore methodologies (~3.7)

[Backup and Restore](https://kubernetes.io/docs/getting-started-guides/ubuntu/backups/)

## Troubleshooting (10%)

### Troubleshoot application failure (~2.5%)

### Troubleshoot control plane failure (~2.5%)

### Troubleshoot worker node failure (~2.5%)

### Troubleshoot networking (~2.5%)

## Application Lifecycle Management (8%)

### Understand Deployments and how to perform rolling updates and rollbacks (~2%)

* https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
* https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#updating-a-deployment
* https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#rolling-back-a-deployment

### Know various ways to configure applications (~2%)

* ConfigMap / Secrets
* env:
* args:

### Know how to scale applications (~2%)

* ReplicaSet
* [Horizontal auto-scale](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)

### Understand the primitives neessary to create a self-healing application (~2%)

* [liveness and readiness](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/)

## Security (12%)

### Know how to configure authentication and authorization (~1.7%)

* [Authentication](https://kubernetes.io/docs/admin/authentication/)
* [Authorization](https://kubernetes.io/docs/admin/authorization/)
* [RBAC](https://kubernetes.io/docs/admin/authorization/rbac/)

### Understand Kubernetes Security Primitives (~1.7%)

* ClusterRole
* ClusterRoleBinding
* See RBAC directly above

### Know how to configure network policies (~1.7%)

* [NetworkPolicy](https://kubernetes.io/docs/concepts/services-networking/network-policies/)

### Create and manage TLS certificates for cluster components (~1.7%)

* [cfssl](https://github.com/cloudflare/cfssl/releases)
* [generate client and server certs](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/02-certificate-authority.md#generate-client-and-server-tls-certificates)

### Work with images securely (~1.7%)

* [Pull from private image registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)

### Define security contexts (~1.7%)

* [security-context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)

### Secure persistent key-value store (~1.7%)

* Secrets

## Networking (11%)

### Understand the networking configuration on the cluster nodes (~1.6)

### Understand Pod networking concepts (~1.6)

### Understand Service networking (~1.6)

### Deploy and configure a network load balancer (~1.6)

### Know how to use Ingress rules (~1.6)

### Know how to configure and use the cluster DNS (~1.6)

### Understand CNI (~1.6)

## Storage (7%)

### Understand Persistent Volumes and know how to create them (~1.4%)

### Understand access modes for PVs (~1.4%)

### Understand Persistent Volume Claims primative (~1.4%)

### Understand Kubernetes storage objects (~1.4%)

### Know how to configure applications with persistent storage (~1.4%)

## Logging/Monitoring (5%)

### Understand how to monitor all cluster components (~1.3%)

### Understand how to monitor applications (~1.3%)

### Manage cluster component logs (~1.3%)

### Manage application logs (~1.3%)

## Installation, Configuration, and Validation (12%)

### Design a Kubernetes Cluster (~1.1%)

### Install Kubernetes masters and nodes (~1.1%)

### Configure Secure cluster communications (~1.1%)

### Configura a highly available kubernetes cluster (~1.1%)

### Know where to get the kubernetes binary releases (~1.1%)

### Provision underlying infrastructure to deploy a kubernetes cluster (~1.1%)

### Choose a network solution (~1.1%)

### Choose your kubernetes infrastructure configuration (~1.1%)

### Run end-to-end tests on your cluster (~1.1%)

### Analyze end-to-end test results (~1.1%)

### Run Node end-to-end tests (~1.1%)

## Scheduling (5%)

### Use label selectors to schedule pods (~0.7%)

### Understand the role of DaemonSets (~0.7%)

### Understand how resource limits could affect pod scheduling (~0.7%)

### Understand how to run multiple schedulers and how to configure pods to use them (~0.7%)

### Manually schedule a pod without a scheduler (~0.7%)

### Display scheduler events (~0.7%)

### Know how to configure the kubernetes scheduler (~0.7%)
